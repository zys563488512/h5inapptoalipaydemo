### iOS App中 webview跳转 url
#### 1. iOS配置请求头

```
alipay://platformapi/startApp?appId=10000011&url=
或者
alipays://platformapi/startApp?appId=10000011&url=
	
```
	
#### 2. 填写订单必要参数x

```
//订单参数
NSDictionary * dict = @{@"merchantOutOrderNo":@"201901160001",@"merid":@"yft2017082500005",@"noncestr":@"test",@"orderMoney":@"1.00",@"orderTime":@"20190116153545",@"notifyUrl":@"http://jh.chinambpc.com/api/callbak"};
```
#### 3. 拼接字符串并对字符串进行md5加密
```
- (NSString *) getString:(NSDictionary *)dic {
    // 通过 key
    NSArray *keys = dic.allKeys;
    NSArray *sortArray = [keys sortedArrayUsingComparator:^NSComparisonResult(id _Nonnull obj1, id _Nonnull obj2) {
        return [obj1 compare: obj2 options:NSNumericSearch];
    }];//对key进行遍历排序
    NSMutableArray *valueArray = [NSMutableArray array];
    for (NSString *sortString in sortArray) {
        [valueArray addObject:[dic objectForKey: sortString]];
    }//对排序后的key取value
    NSMutableArray *signArray = [NSMutableArray array];
    for (int i =0; i < sortArray.count; i++) {
        NSString *keyValueStr = [NSString stringWithFormat:@"%@=%@",sortArray[i],valueArray[i]];
        [signArray addObject: keyValueStr];
    }//输出新的数组   key=value
    NSString *stringA = [signArray componentsJoinedByString:@"&"];
    //拼接加密字串signKey
    NSString * stringSignTemp = [NSString stringWithFormat:@"%@&key=%@",stringA,signKey];
    NSString * sign = [self md5:stringSignTemp];
    
    NSString * signStr = [NSString stringWithFormat:@"https://alipay.3c-buy.com/api/createOrder?%@&sign=%@",stringA,sign];
    NSString *charactersToEscape = @"?!@#$^&%*+,:;='\"`<>()[]{}/\\| ";
    NSCharacterSet *allowedCharacters = [[NSCharacterSet characterSetWithCharactersInString:charactersToEscape] invertedSet];
    NSString *encodeValue = [signStr stringByAddingPercentEncodingWithAllowedCharacters:allowedCharacters];
    return encodeValue;
}
```
* 注意：加密字串需定义

```
#define signKey @"gNociwieX1aCSkhvVemcXkaF9KVmkXm8"
```

#### md5 小写加密
```
-(NSString *)md5:(NSString *)str1 //iOS原生的MD5加密方法
{
    const char *str = str1.UTF8String;
    if (str == NULL) {
        str = "";
    }
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(str,strlen(str), result); // This is the md5 call
    return [NSString stringWithFormat:@"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",result[0], result[1], result[2], result[3],result[4], result[5], result[6], result[7],result[8], result[9], result[10], result[11],result[12], result[13], result[14], result[15]];
}
```
* 注意：需要引入头文件 

```
 #import <CommonCrypto/CommonDigest.h> 
```

#### 4. 拼接跳转到支付宝
```
//拼接跳转机制
    NSString* aliPayInfo = [NSString stringWithFormat:@"%@%@",@"alipays://platformapi/startApp?appId=10000011&url=",[self getString:dict]];
    NSURL *url = [NSURL URLWithString:aliPayInfo];
    [[UIApplication sharedApplication]openURL:url options:@{} completionHandler:nil];
```
详细代码请查看demo: 
[https://gitlab.com/zys563488512/h5inapptoalipaydemo.git](https://gitlab.com/zys563488512/h5inapptoalipaydemo.git)