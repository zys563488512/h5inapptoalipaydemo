//
//  main.m
//  alipayDemo
//
//  Created by yifutong on 2019/1/15.
//  Copyright © 2019 yifutong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
