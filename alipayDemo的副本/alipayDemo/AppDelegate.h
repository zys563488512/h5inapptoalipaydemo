//
//  AppDelegate.h
//  alipayDemo
//
//  Created by yifutong on 2019/1/15.
//  Copyright © 2019 yifutong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

