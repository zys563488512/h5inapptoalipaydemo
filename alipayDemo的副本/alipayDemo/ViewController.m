//
//  ViewController.m
//  alipayDemo
//
//  Created by yifutong on 2019/1/15.
//  Copyright © 2019 yifutong. All rights reserved.
//

#import "ViewController.h"
#import <CommonCrypto/CommonDigest.h>
#import "WXApi.h"
#define signKey @"gNociwieX1aCSkhvVemcXkaF9KVmkXm8"
@interface ViewController () <UIApplicationDelegate, WXApiDelegate>

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}
- (IBAction)aliPayClick:(id)sender {
    [self getAliPay];
}
- (IBAction)weixinPayClick:(id)sender {
    [self getWeiXin];
}
- (IBAction)wxHPayClick:(id)sender {
//   JumpToBizProfileReq *req = [JumpToBizProfileReq new];
//　　req.username = @"gh_e86e63bf3b6f"; // 原始ID
//　　req.profileType = WXBizProfileType_Normal;
//　　req.extMsg = @"";
//　　[WXApi sendReq:req];
    
    OpenWebviewReq *open = [[OpenWebviewReq alloc] init];
    open.url = @"http://mp.weixin.qq.com/wiki/17/c0f37d5704f0b64713d5d2c37b468d75.html";
    open.openID = @"AQEk2TOrUMtiaBeVNxluojpbwYImC19h";
    [WXApi sendReq:open];
//    WXWebpageObject *webpageObject = [WXWebpageObject object];
//    webpageObject.webpageUrl = @"http://www.baidu.com";
    
//    WXMediaMessage *message = [WXMediaMessage message];
//    message.title = @"标题";
//    message.description = @"描述";
//    [message setThumbImage:[UIImage imageNamed:@"缩略图.jpg"]];
//    message.mediaObject = webpageObject;
//
//    SendMessageToWXReq *req = [[SendMessageToWXReq alloc] init];
//    req.bText = NO;
//    req.message = message;
//    req.scene = WXSceneSession;
//    [WXApi sendReq:req];
}


//微信常规支付
- (void) getWeiXin {
    //订单参数
    //wxe4df60c29783df7d
    //wx2421b1c4370ec43b
    //nonceStr 随机字串  微信返回的随机字符串
    //prepayid 预支付交易会话标识  微信生成的预支付会话标识，用于后续接口调用中使用，该值有效期为2小时
    //package web
    NSDictionary * dict = @{@"appid":@"wxe4df60c29783df7d",@"timeStamp":[self getNowTimeTimestamp],@"prepayid":@"wx15193804290142b4f840d32b1511685057",@"nonceStr":@"3e84679af4efab5f32ee9ea01b2ec290",@"package":@"web"};

    //拼接跳转机制
    NSString* wxPayInfo = [NSString stringWithFormat:@"%@%@",@"weixin://wap/pay?",[self getWeiXinString:dict]];
//    NSURL *url = [NSURL URLWithString:@"weixin://wap/pay?appid%3Dwx2421b1c4370ec43b%26noncestr%3D3e84679af4efab5f32ee9ea01b2ec290%26package%3DWAP%26prepayid%3Dwx20160504154919fdacd7bc0d0127918780%26timestamp%3D1462348159%26sign%3DC40DC4BB970049D6830BA567189B463B"];
    NSURL *url = [NSURL URLWithString:wxPayInfo];

    [[UIApplication sharedApplication]openURL:url options:@{} completionHandler:nil];
    
}
- (NSString *) getWeiXinString:(NSDictionary *)dic {
    // 通过 key
    NSArray *keys = dic.allKeys;
    NSArray *sortArray = [keys sortedArrayUsingComparator:^NSComparisonResult(id _Nonnull obj1, id _Nonnull obj2) {
        return [obj1 compare: obj2 options:NSNumericSearch];
    }];//对key进行遍历排序
    NSMutableArray *valueArray = [NSMutableArray array];
    for (NSString *sortString in sortArray) {
        [valueArray addObject:[dic objectForKey: sortString]];
    }//对排序后的key取value
    NSMutableArray *signArray = [NSMutableArray array];
    for (int i =0; i < sortArray.count; i++) {
        NSString *keyValueStr = [NSString stringWithFormat:@"%@=%@",sortArray[i],valueArray[i]];
        [signArray addObject: keyValueStr];
    }//输出新的数组   key=value
    NSLog(@"signArray====%@",signArray);
    NSString *stringA = [signArray componentsJoinedByString:@"&"];
    NSLog(@"stringA==%@",stringA);
    NSString * stringSignTemp = [NSString stringWithFormat:@"%@&key=%@",stringA,signKey];
    NSLog(@"stringSignTemp==%@",stringSignTemp);
    NSString * sign = [self md5X:stringSignTemp];
    NSLog(@"sign==%@",sign);
    
    NSString * signStr = [NSString stringWithFormat:@"%@&sign=%@",stringA,sign];
    NSString *charactersToEscape = @"?!@#$^&%*+,:;='\"`<>()[]{}/\\| ";
    NSCharacterSet *allowedCharacters = [[NSCharacterSet characterSetWithCharactersInString:charactersToEscape] invertedSet];
    NSString *encodeValue = [signStr stringByAddingPercentEncodingWithAllowedCharacters:allowedCharacters];
    NSLog(@"encodeValue----%@",encodeValue);
    return encodeValue;
}

//支付宝
- (void) getAliPay {
    //订单参数
    NSDictionary * dict = @{@"merchantOutOrderNo":@"201901160001",@"merid":@"yft2017082500005",@"noncestr":@"test",@"orderMoney":@"1.00",@"orderTime":@"20190116153545",@"notifyUrl":@"http://jh.chinambpc.com/api/callbak"};
    //拼接跳转机制
    NSString* aliPayInfo = [NSString stringWithFormat:@"%@%@",@"alipays://platformapi/startApp?appId=10000011&url=",[self getString:dict]];
    NSURL *url = [NSURL URLWithString:aliPayInfo];
    [[UIApplication sharedApplication]openURL:url options:@{} completionHandler:nil];
    
}
- (NSString *) getString:(NSDictionary *)dic {
    // 通过 key
    NSArray *keys = dic.allKeys;
    NSArray *sortArray = [keys sortedArrayUsingComparator:^NSComparisonResult(id _Nonnull obj1, id _Nonnull obj2) {
        return [obj1 compare: obj2 options:NSNumericSearch];
    }];//对key进行遍历排序
    NSMutableArray *valueArray = [NSMutableArray array];
    for (NSString *sortString in sortArray) {
        [valueArray addObject:[dic objectForKey: sortString]];
    }//对排序后的key取value
    NSMutableArray *signArray = [NSMutableArray array];
    for (int i =0; i < sortArray.count; i++) {
        NSString *keyValueStr = [NSString stringWithFormat:@"%@=%@",sortArray[i],valueArray[i]];
        [signArray addObject: keyValueStr];
    }//输出新的数组   key=value
    NSLog(@"signArray====%@",signArray);
    NSString *stringA = [signArray componentsJoinedByString:@"&"];
    NSLog(@"stringA==%@",stringA);
    NSString * stringSignTemp = [NSString stringWithFormat:@"%@&key=%@",stringA,signKey];
    NSLog(@"stringSignTemp==%@",stringSignTemp);
    NSString * sign = [self md5:stringSignTemp];
    NSLog(@"sign==%@",sign);
    
    NSString * signStr = [NSString stringWithFormat:@"https://alipay.3c-buy.com/api/createOrder?%@&sign=%@",stringA,sign];
    NSString *charactersToEscape = @"?!@#$^&%*+,:;='\"`<>()[]{}/\\| ";
    NSCharacterSet *allowedCharacters = [[NSCharacterSet characterSetWithCharactersInString:charactersToEscape] invertedSet];
    NSString *encodeValue = [signStr stringByAddingPercentEncodingWithAllowedCharacters:allowedCharacters];
    NSLog(@"encodeValue----%@",encodeValue);
    return encodeValue;
}
-(NSString *)md5:(NSString *)str1 //iOS原生的MD5加密方法
{
    const char *str = str1.UTF8String;
    if (str == NULL) {
        str = "";
    }
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(str,strlen(str), result); // This is the md5 call
    return [NSString stringWithFormat:@"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",result[0], result[1], result[2], result[3],result[4], result[5], result[6], result[7],result[8], result[9], result[10], result[11],result[12], result[13], result[14], result[15]];
}

-(NSString *)md5X:(NSString *)str1 //iOS原生的MD5加密方法
{
    const char *str = str1.UTF8String;
    if (str == NULL) {
        str = "";
    }
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(str,strlen(str), result); // This is the md5 call
    return [NSString stringWithFormat:@"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",result[0], result[1], result[2], result[3],result[4], result[5], result[6], result[7],result[8], result[9], result[10], result[11],result[12], result[13], result[14], result[15]];
}

//获取当前时间戳有两种方法(以秒为单位)

-(NSString *)getNowTimeTimestamp{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"]; // ----------设置你想要的格式,hh与HH的区别:分别表示12小时制,24小时制
    
    //设置时区,这个对于时间的处理有时很重要
    
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Shanghai"];
    
    [formatter setTimeZone:timeZone];
    
    NSDate *datenow = [NSDate date];//现在时间,你可以输出来看下是什么格式
    
    NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)[datenow timeIntervalSince1970]];
    
    return timeSp;
    
}

//获取当前时间戳  （以毫秒为单位）

+(NSString *)getNowTimeTimestamp3{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss SSS"]; // ----------设置你想要的格式,hh与HH的区别:分别表示12小时制,24小时制
    
    //设置时区,这个对于时间的处理有时很重要
    
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Shanghai"];
    
    [formatter setTimeZone:timeZone];
    
    NSDate *datenow = [NSDate date];//现在时间,你可以输出来看下是什么格式
    
    NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)[datenow timeIntervalSince1970]*1000];
    
    return timeSp;
    
}
@end
